
use <moslib/libchamfer.scad>;
include <peglib/defaults.scad>;
include <peglib/rail.scad>;


wideness = 2;

$fn = 90;

color("blue") {
     rail(height = 2, chamfer = wall_thickness / 4);
}


color("salmon") {
     intersection() {
          translate([0, 0, 0]) {

               // box post
               rotate([-45, 0, 0]) {
                    chamfered_box([base_size / 2, base_size / 2, base_size * 1.5], [0, -1, 1], chamfer = base_size / 8);
               }

          }
          translate([0, wall_thickness / 2, 0]) {
            chamfered_box(dim = [base_size, base_size * 2, base_size * 2], align = [0, 1, 1]);
          }
     }
}



