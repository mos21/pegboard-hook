
use <moslib/libchamfer.scad>;
include <peglib/defaults.scad>;
include <peglib/rail.scad>;



a = 45;
l = 2;
w = 3/4;

$fn = 90;

module base(height, copies) {
    color("blue") {
        rail(height = height, chamfer = wall_thickness / 4, copies = copies);
    }
}







module post(longness, wideness, angle) {

    rotate([0, 0, 180]) 
        {

        color("salmon") {
            rotate([angle, 0, 0]) {
                chamfered_box([base_size * wideness, base_size * wideness, base_size * longness], [0, 0, 1], chamfer = (base_size * wideness) / 4);
            }
        }

    }


}



module shoulder(longness, wideness, angle) {

    // color("red") cylinder(h = extent * base_size);

    intersection() 
    {
        hull() 
        {
            post(longness, wideness,  angle);
            post(longness, wideness, -angle);
        }

        color("grey")
        union() {
            chamfered_box([wideness * base_size, longness * base_size, longness * base_size], [0, 1, 1], chamfer = wall_thickness / 4);
        }

    }

}


module finger(longness, wideness, angle) {
    
    intersection() 
    {
        post(longness, wideness, angle);
    
        color("grey")
        union() {
            chamfered_box([wideness * base_size, longness * base_size, longness * base_size], [0, 1, 1], chamfer = wall_thickness / 4);
        }
    }        
}

shoulder(longness = l * (2/3), wideness = w, angle = a);
finger(longness = l, wideness = w, angle = a);
translate([0, -wall_thickness / 4, 0]) base(floor((l + 2) / 2), 1);





