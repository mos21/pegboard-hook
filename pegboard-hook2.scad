
use <moslib/libchamfer.scad>;
include <peglib/defaults.scad>;
include <peglib/rail.scad>;


wideness = 2;
    girth = 2;

$fn = 90;

shoulder = true;


module base() {
    color("blue") {
         rail(height = 3, chamfer = wall_thickness / 4, copies = 2);
    }
}



module post() {
    color("salmon") {
        intersection() {

translate([0, 0, -(base_size - (wall_thickness * 0))])
            union() {
                // box post
                rotate([-45, 0, 0]) {
                    chamfered_box([(base_size / 2) * girth, (base_size / 2) * girth, (base_size * 2) * girth], [0, -1, 1], chamfer = base_size / 8);
                }

                //shoulder
                if(shoulder) {
                    hull() {
                        rotate([-45, 0, 0]) {
                            chamfered_box([(base_size / 2) * girth, (base_size / 2) * girth, (base_size * 1.5) * girth], [0, -1, 1], chamfer = base_size / 8);
                        }
                        rotate([45, 0, 0]) {
                            chamfered_box([(base_size / 2) * girth, (base_size / 2) * girth, (base_size * 1.5) * girth], [0, 1, 1], chamfer = base_size / 8);
                        }
                    }
                }
            }
            translate([0, wall_thickness / 2, 0]) {
                chamfered_box(dim = [(base_size / 2) * girth, (base_size * 2) * girth, (base_size * 2) * girth], align = [0, 1, 1]);
            }
        }
    }
}




base();
post();
