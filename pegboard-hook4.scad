
use <moslib/libchamfer.scad>;
include <peglib/defaults.scad>;
include <peglib/rail.scad>;




module post(length, width, angle = 45, shoulder = true, shoulder_height_ratio = 2/3, spike = false, spike_height = 0) {
     c = (width * base_size) / 4;

     // figure out the geometry later, if we feel the need
     assert(angle == 45);

     // distance from centerline
     d = sqrt(pow(length * base_size, 2) / 2);

     // walk back the chamfer depth
     wb = sqrt(pow(c, 2) / 2);
                    

     intersection() {

          translate([0, 0, -(base_size * 1/2)])
          union() {
               
               // box post
               color("salmon")
               rotate([-angle, 0, 0]) {
                    chamfered_box([width * base_size, width * base_size, length * base_size], [0, -1, 1], chamfer = c);
               }
                    
               // spike
               color("green")
               if(spike) {
                    translate([0, d - wb, d - wb]) 
                    chamfered_box([width * base_size, width * base_size, (((spike_height ? spike_height : length) * base_size) + (base_size * 1/2)) - (d - wb)], [0, -1, 1], chamfer = c);
                    
               }

               //shoulder
               color("orange")
               if(shoulder) {
                    hull() {
                         rotate([-angle, 0, 0]) {
                              chamfered_box([width * base_size, width * base_size, length * base_size * shoulder_height_ratio], [0, -1, 1], chamfer = c);
                         }
                         rotate([angle, 0, 0]) {
                              chamfered_box([width * base_size, width * base_size, length * base_size * shoulder_height_ratio], [0, 1, 1], chamfer = c);
                         }
                    }
               }
          }

          color("pink")
               translate([0, wall_thickness / 2, 0]) {
               // the first box is to get the chamfer correct on the bottom of the post
               chamfered_box(dim = [width * base_size, ((d - wb) * 2) - c, (spike_height ? spike_height : length) * base_size ], align = [0, 0, 1], chamfer = c);
          }
               
          color("red")
               translate([0, wall_thickness / 2, 0]) {
               // the second box is to end the post/shoulder in the middle of the rail cap
               chamfered_box(dim = [width * base_size, length * base_size, (spike_height ? spike_height : length) * base_size ], align = [0, 1, 1]);
          }
     }
}




rail_wideness = 2;
hook_wideness_ration = 1/2;

girth = 2;

$fn = 90;

shoulder = true;



color("blue") rail(height = 3, chamfer = wall_thickness / 4, copies = 1);

translate([0, 0, 0.001])
post(length = 2, width = 1/2, spike = true, spike_height = 2.5);

